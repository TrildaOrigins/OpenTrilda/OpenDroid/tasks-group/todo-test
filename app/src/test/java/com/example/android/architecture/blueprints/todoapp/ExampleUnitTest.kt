package com.example.android.architecture.blueprints.todoapp

import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        MatcherAssert.assertThat(4, CoreMatchers.`is`(2+ 2))
    }
}
