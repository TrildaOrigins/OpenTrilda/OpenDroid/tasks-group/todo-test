package com.example.android.architecture.blueprints.todoapp.addedittask

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.architecture.blueprints.todoapp.data.source.iTasksRepository

@Suppress("UNCHECKED_CAST")
class  AddEditTaskViewModelFactory (
    private val tasksRepository: iTasksRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>) =
        (AddEditTaskViewModel(tasksRepository) as T)
}
